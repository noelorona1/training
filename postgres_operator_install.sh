#!/bin/bash
#these variables are used for the install of postgres operator
TDS_VERSION="1.5.0"
MY_REGISTRY="harbor.h2o-2-1546.h2o.vmware.com/tds-packages"
MY_REGISTRY_USERNAME='robot$tap'
MY_REGISTRY_PASSWORD=""
REGISTRY_SECRET="harbor-creds"
PACKAGE_NAME="postgres-operator"

#Ensure you perform a docker login to registry.tanzu.vmware.com
#docker login to the registry you need to push images to

imgpkg copy -b registry.tanzu.vmware.com/packages-for-vmware-tanzu-data-services/tds-packages:${TDS_VERSION} \
  --to-repo ${MY_REGISTRY}/tds-packages \
  --registry-ca-cert-path '/home/jumpbox/Projects/tactical-p2p/certificates/H2O/harbor/harbor-ca.crt'


tanzu secret registry add ${REGISTRY_SECRET} \
  --username ${MY_REGISTRY_USERNAME} \
  --password ${MY_REGISTRY_PASSWORD} \
  --server ${MY_REGISTRY} \
  --export-to-all-namespaces --yes

tanzu package repository add tanzu-data-services-repository --url ${MY_REGISTRY}/tds-packages:${TDS_VERSION}


#Ensure you set the correct values in the values-override.yaml that will get applied here.
tanzu package install ${PACKAGE_NAME} \
--package-name postgres-operator.sql.tanzu.vmware.com \
--version 1.8.0 -f ./values-override.yaml